import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.junit.JUnitSpekRunner
import org.junit.runner.RunWith
import kotlin.test.assertEquals

@RunWith(JUnitSpekRunner::class)
class NonsenseTestableSpec : Spek({
    describe("a testable function") {
        it("should return true") {
            val testResult = testable()
            assertEquals(true, testResult)
            println("Proof that 'should return true' ran")
        }
    }
})
